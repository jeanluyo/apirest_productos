package com.entregable1.apirest_productos.service;

import com.entregable1.apirest_productos.model.Producto;
import com.entregable1.apirest_productos.model.Usuario;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class ProductoService {

    //Persistencia de productos
    private static final List<Producto> productoList = new ArrayList<>();

    //Identificadores Incrementales
    private final AtomicInteger secuenciaIdsProductos = new AtomicInteger(0);
    private final AtomicInteger secuenciaIdsUsuarios = new AtomicInteger(0);

    public List<Producto> getProductos() {
        //Probando listas no modificables...
        return Collections.unmodifiableList(productoList);
    }

    public Producto getProducto(int id) {
        for (Producto producto : productoList) {
            if (producto.id == id) {
                return producto;
            }
        }
        return null;
    }

    public void addProducto(Producto producto) {
        producto.id = this.secuenciaIdsProductos.incrementAndGet();

        // Cuando enviemos nombres de usuarios se deben de generar Ids correlativos por cada uno de ellos...
        if (producto.usuarios != null && producto.usuarios.size() > 0) {
            producto.usuarios.forEach(usuarioModel -> usuarioModel.id = secuenciaIdsUsuarios.incrementAndGet());
        }
        productoList.add(producto);
    }

    public void updateProducto(int id, Producto productToUpdate) {
        for (int i = 0; i < productoList.size(); i++) {
            Producto producto = productoList.get(i);
            if (producto.id == id) {
                //Reemplazamos el id del producto con el mismo Id de entrada.
                productToUpdate.id = id;
                productoList.set(i, productToUpdate);
                return;
            }
        }
    }

    public void removeProducto(int id) {
        for (Producto producto: productoList) {
            if (producto.id == id) {
                productoList.remove(producto);
                return;
            }
        }
    }

    public Usuario addUsuarioProducto(int idProducto, Usuario usuario) {
        Producto producto = this.getProducto(idProducto);
        if (producto != null) {
            usuario.id = this.secuenciaIdsUsuarios.incrementAndGet();
            producto.usuarios.add(usuario);
            return usuario;
        }

        return null;
    }

    public Usuario updateUsuarioProducto(int idProducto, int idUsuario, Usuario usuarioToUpdate) {
        Producto producto = this.getProducto(idProducto);
        if (producto != null) {
            //Recorremos listado de usuarios
            for (int i = 0; i < producto.usuarios.size(); i++) {
                Usuario usuario = producto.usuarios.get(i);
                // Buscamos el ID del usuario a actualizar
                if (usuario.id == idUsuario) {
                    //actualizamos el id del usuario que reemplazará al antiguo
                    usuarioToUpdate.id = idUsuario;
                    producto.usuarios.set(i, usuarioToUpdate);
                    return usuarioToUpdate;
                }
            }
        }
        return null;
    }

    public Producto deleteUsuarioProducto(int idProducto, int idUsuario) {
        Producto producto = this.getProducto(idProducto);
        if (producto != null) {
            //Recorremos listado de usuarios
            for (int i = 0; i < producto.usuarios.size(); i++) {
                Usuario usuario = producto.usuarios.get(i);
                // Buscamos el ID del usuario a eliminar
                if (usuario.id == idUsuario) {
                    //eliminamos el usuario encontrado
                    producto.usuarios.remove(usuario);
                    return producto;
                }
            }
        }
        return null;
    }
}
