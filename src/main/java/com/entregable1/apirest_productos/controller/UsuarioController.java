package com.entregable1.apirest_productos.controller;

import com.entregable1.apirest_productos.model.Producto;
import com.entregable1.apirest_productos.model.Usuario;
import com.entregable1.apirest_productos.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${api.version}/productos")
public class UsuarioController {
    @Autowired
    private ProductoService productService;

    @GetMapping("/{idProducto}/usuarios")
    public ResponseEntity getProductIdUsers(@PathVariable int idProducto) {
        Producto producto = productService.getProducto(idProducto);
        if (producto == null) {
            return new ResponseEntity<>("Producto no encontrado...", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(producto.usuarios);
    }

    @PostMapping("/{idProducto}/usuarios")
    public ResponseEntity addUsuarioProducto(@PathVariable int idProducto,
                                             @RequestBody Usuario usuarioEntrada) {
        Usuario usuario = productService.addUsuarioProducto(idProducto, usuarioEntrada);
        if (usuario == null) {
            return new ResponseEntity<>("Producto no encontrado...", HttpStatus.NOT_FOUND);
        } else {
            return ResponseEntity.ok(usuario);
        }
    }

    @PutMapping("/{idProducto}/usuarios/{idUsuario}")
    public ResponseEntity updateUsuarioProducto(@PathVariable int idProducto,
                                         @PathVariable int idUsuario,
                                         @RequestBody Usuario usuarioToUpdate) {
        Usuario usuario = productService.updateUsuarioProducto(idProducto, idUsuario, usuarioToUpdate);
        if (usuario == null) {
            return new ResponseEntity<>("Producto no encontrado...", HttpStatus.NOT_FOUND);
        } else {
            return ResponseEntity.ok(usuario);
        }
    }

    @DeleteMapping("/{idProducto}/usuarios/{idUsuario}")
    public ResponseEntity deleteProducto(@PathVariable int idProducto, @PathVariable int idUsuario) {
        Producto pr = productService.deleteUsuarioProducto(idProducto, idUsuario);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado...", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
