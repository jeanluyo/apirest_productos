package com.entregable1.apirest_productos.controller;

import com.entregable1.apirest_productos.model.Producto;
import com.entregable1.apirest_productos.model.ProductoPrecioOnly;
import com.entregable1.apirest_productos.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${api.version}/productos")
public class ProductoController {

    @Autowired
    private ProductoService productService;

    @GetMapping
    public ResponseEntity getProductos() {
        List<Producto> productos = productService.getProductos();
        return ResponseEntity.ok(productos);
    }

    @GetMapping("/{id}")
    public ResponseEntity getProductoPorId(@PathVariable int id) {
        Producto pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado...", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    @PostMapping
    public ResponseEntity addProducto(@RequestBody Producto producto) {
        productService.addProducto(producto);
        return new ResponseEntity<>("Producto creado satisfactoriamente!", HttpStatus.CREATED);
        //Podriamos tambien regresar un 200 y el producto creado...
        //return ResponseEntity.ok(producto);
    }
    @PutMapping("/{id}")
    public ResponseEntity updateProducto(@PathVariable int id,
                                         @RequestBody Producto productToUpdate) {
        Producto pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado...", HttpStatus.NOT_FOUND);
        }
        productService.updateProducto(id, productToUpdate);
        return new ResponseEntity<>("Producto actualizado correctamente...", HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteProducto(@PathVariable int id) {
        Producto pr = productService.getProducto(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado...", HttpStatus.NOT_FOUND);
        }
        productService.removeProducto(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping("/{id}")
    public ResponseEntity patchPrecioProducto(
            @RequestBody ProductoPrecioOnly productoPrecioOnly, @PathVariable int id){
        Producto pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado...", HttpStatus.NOT_FOUND);
        }
        pr.precio = productoPrecioOnly.precio;
        productService.updateProducto(id, pr);
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }

}
