package com.entregable1.apirest_productos.model;

import java.util.ArrayList;
import java.util.List;

public class Producto {
    public int id;
    public String descripcion;
    public String categoria;
    public double precio;
    public List<Usuario> usuarios = new ArrayList<>();
}
